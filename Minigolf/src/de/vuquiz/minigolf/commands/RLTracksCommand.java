package de.vuquiz.minigolf.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.vuquiz.minigolf.Minigolf;

public class RLTracksCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		// checking if command was issued by a player
		if (sender instanceof Player) {
			Player player = (Player) sender; // command sender

			// checking for permission
			if (player.hasPermission("minigolf.setup")) {
				// reloading the golf tracks
				Minigolf.loadTracks();
				player.sendMessage(
						Minigolf.getPrefix() + ChatColor.GREEN + "Alle Bahnen wurden erfolgreich neu geladen.");
			}
		}
		return true;
	}
}