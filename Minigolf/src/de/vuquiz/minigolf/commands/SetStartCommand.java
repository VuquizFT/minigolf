package de.vuquiz.minigolf.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.vuquiz.minigolf.Minigolf;

public class SetStartCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		// checking if command was issued by a player
		if (sender instanceof Player) {
			Player player = (Player) sender; // command sender

			// checking for permission
			if (player.hasPermission("minigolf.setup")) {
				// checking for right syntax
				if (args.length == 1) {
					// golf track id
					int track = 0;

					try { // determining track number
						track = Integer.parseInt(args[0]);
					} catch (NumberFormatException numberFormatException) {
						// given value is not a number
						player.sendMessage(
								Minigolf.getPrefix() + ChatColor.RED + "Der übergebene Wert muss eine Zahl sein.");
						return false; // do not go any further here
					}
					// checking for proper track number
					if (track > 0) {
						// saving track start location
						Minigolf.getTrackConfig().saveStart(track, player.getLocation());
						player.sendMessage(
								Minigolf.getPrefix() + ChatColor.GREEN + "Du hast den Start für die " + ChatColor.GOLD
										+ "Bahn " + String.valueOf(track) + ChatColor.GREEN + " erfolgreich gesetzt.");
					} else {
						player.sendMessage(Minigolf.getPrefix() + ChatColor.RED + "Bitte gebe eine korrekte Bahn an.");
					}
				} else {
					player.sendMessage(Minigolf.getPrefix() + ChatColor.RED
							+ "Bitte verwende /setstart <Bahn> - setzt die Start Position für eine Bahn");
				}
			}
		}
		return true;
	}
}