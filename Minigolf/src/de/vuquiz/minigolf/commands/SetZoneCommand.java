package de.vuquiz.minigolf.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.vuquiz.minigolf.Minigolf;

public class SetZoneCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		// checking if command was issued by a player
		if (sender instanceof Player) {
			Player player = (Player) sender; // command sender

			// checking for permission
			if (player.hasPermission("minigolf.setup")) {
				if (args.length == 2) {
					// golf track id
					int track = 0;

					try { // determining track number
						track = Integer.parseInt(args[0]);
					} catch (NumberFormatException numberFormatException) {
						// given value is not a number
						player.sendMessage(
								Minigolf.getPrefix() + ChatColor.RED + "Der übergebene Wert muss eine Zahl sein.");
						return false; // do not go any further here
					}

					// zone id
					int zone = 0;

					try { // determining zone number
						zone = Integer.parseInt(args[1]);
					} catch (NumberFormatException numberFormatException) {
						// given value is not a number
						player.sendMessage(
								Minigolf.getPrefix() + ChatColor.RED + "Der übergebene Wert muss eine Zahl sein.");
						return false; // do not go any further here
					}
					// checking for proper zone- and track number
					if ((zone == 1 || zone == 2) && track > 0) {
						// saving zone location
						Minigolf.getTrackConfig().saveZone(track, zone, player.getLocation());
						player.sendMessage(Minigolf.getPrefix() + ChatColor.GREEN + "Du hast die Position "
								+ ChatColor.GOLD + String.valueOf(zone) + ChatColor.GREEN + " für die Bahn "
								+ ChatColor.GOLD + String.valueOf(track) + ChatColor.GREEN + " erfolgreich gesetzt.");
					} else {
						player.sendMessage(
								Minigolf.getPrefix() + ChatColor.RED + "Bitte wähle entweder Zone 1 oder Zone 2.");
					}
				} else {
					player.sendMessage(Minigolf.getPrefix() + ChatColor.RED
							+ "Bitte verwende /setzone <Bahn> <Zone> - setzt den Bereich für eine Bahn");
				}
			}
		}
		return true;
	}
}