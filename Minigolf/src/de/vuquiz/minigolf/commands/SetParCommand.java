package de.vuquiz.minigolf.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.vuquiz.minigolf.Minigolf;

public class SetParCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		// checking if command was issued by a player
		if (sender instanceof Player) {
			Player player = (Player) sender; // command sender

			// checking for permission
			if (player.hasPermission("minigolf.setup")) {
				// checking for right syntax
				if (args.length == 2) {
					// golf track id
					int track = 0;

					try { // determining track number
						track = Integer.parseInt(args[0]);
					} catch (NumberFormatException numberFormatException) {
						// given value is not a number
						player.sendMessage(
								Minigolf.getPrefix() + ChatColor.RED + "Der übergebene Wert muss eine Zahl sein.");
						return false; // do not go any further here
					}
					// par amount
					int par = 0;

					try { // determining par amount
						par = Integer.parseInt(args[1]);
					} catch (NumberFormatException numberFormatException) {
						// given value is not a number
						player.sendMessage(
								Minigolf.getPrefix() + ChatColor.RED + "Der übergebene Wert muss eine Zahl sein.");
						return false; // do not go any further here
					}
					// checking for proper track- and par number
					if (track > 0 && par > 0) {
						// saving par amount for given track
						Minigolf.getTrackConfig().savePar(track, par);
						player.sendMessage(
								Minigolf.getPrefix() + ChatColor.GREEN + "Du hast die Par Anzahl für die Bahn "
										+ ChatColor.GOLD + String.valueOf(track) + ChatColor.GREEN + " erfolgreich auf "
										+ ChatColor.GOLD + String.valueOf(par) + ChatColor.GREEN + " gesetzt.");
					} else {
						player.sendMessage(
								Minigolf.getPrefix() + ChatColor.RED + "Die Bahn oder Par Anzahl ist ungültig.");
					}
				} else {
					player.sendMessage(Minigolf.getPrefix() + ChatColor.RED
							+ "Bitte verwende /setzone <Bahn> <Zone> - setzt den Bereich für eine Bahn");
				}
			}
		}
		return true;
	}
}