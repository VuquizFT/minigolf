package de.vuquiz.minigolf.api;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class ActionbarAPI {

	/**
	 * sends PacketPlayOutChat packet to send a specific action bar to the given
	 * player
	 * 
	 * @param player  the player who should receive the action bar
	 * @param message the contents of the action bar
	 */
	public static void sendActionbar(Player player, String message) {
		// editing message (e.g. the color codes)
		final String newMessage = message.replace("_", " ");
		String submit = ChatColor.translateAlternateColorCodes('&', newMessage);

		// sending action bar packet with given message
		IChatBaseComponent icbc = ChatSerializer.a("{\"text\": \"" + submit + "\"}");
		PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte) 2);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(bar);
	}
}