package de.vuquiz.minigolf.listener;

import de.vuquiz.minigolf.Minigolf;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PlayerPickUpItemListener implements Listener {

	@EventHandler
	public void onPlayerPickUpItem(PlayerPickupItemEvent event) {
		// player, triggering the event
		final Player player = event.getPlayer();

		// player should not pick up items (e.g. the golfball) if ingame
		if (Minigolf.getInstance().getGolfplayer(player) != null)
			// cancelling event
			event.setCancelled(true);
	}
}
