package de.vuquiz.minigolf.listener;

import de.vuquiz.minigolf.Minigolf;
import de.vuquiz.minigolf.util.GolfPlayer;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class PlayerInteractListener implements Listener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		// player that interacts with something
		final Player player = event.getPlayer();

		// right- and left click interaction
		if (event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_AIR
				|| event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
			// some checks to avoid null pointer exceptions
			if (event.getItem() != null) {
				if (event.getItem().hasItemMeta()) {
					// if player uses golf club
					if (event.getItem().getItemMeta().getDisplayName()
							.equalsIgnoreCase(ChatColor.GOLD + "Golfschläger")) {
						// the golf player object of the current player
						final GolfPlayer golfPlayer = Minigolf.getInstance().getGolfplayer(player);

						// if player is currently using a track
						if (golfPlayer != null) {
							// if player has not already finished this track
							if (!golfPlayer.getGolfTrack().getIgnore().contains(player.getUniqueId())) {
								// if player is loading the hit
								if (golfPlayer.isLoading()) {
									// committing shot
									Vector vector = player.getLocation().getDirection();
									if (golfPlayer.getStrength() <= 1) {
										vector.multiply(1);
									} else {
										vector.multiply(golfPlayer.getStrength());
									}
									golfPlayer.shoot(vector);
								} else {
									// starting to load up shot strength
									golfPlayer.loadStrength();
								}
							}
						}
					}
				}
			}
		}
	}
}
