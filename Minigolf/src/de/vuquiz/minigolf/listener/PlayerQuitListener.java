package de.vuquiz.minigolf.listener;

import de.vuquiz.minigolf.Minigolf;
import de.vuquiz.minigolf.util.GolfPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        // the player leaving the server
        final Player player = event.getPlayer();
        // the corresponding golf player object of the leaving player
        final GolfPlayer golfPlayer = Minigolf.getInstance().getGolfplayer(player);

        // player is still ingame (and thus the object not null)
        if (golfPlayer != null) {
            // removing player from current golf track
            Minigolf.getGolfTrack(golfPlayer.getGolfTrack().getID()).removePlayer(player);
            Minigolf.getGolfTrack(golfPlayer.getGolfTrack().getID()).getIgnore().remove(player.getUniqueId());
        }
    }
}
