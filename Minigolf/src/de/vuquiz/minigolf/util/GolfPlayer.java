package de.vuquiz.minigolf.util;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.util.Vector;

import de.vuquiz.minigolf.Minigolf;
import de.vuquiz.minigolf.api.ActionbarAPI;
import de.vuquiz.minigolf.api.TitleAPI;
import net.md_5.bungee.api.ChatColor;

public class GolfPlayer {

	// instance variables
	private UUID player;
	private Item golfball;
	private GolfTrack golfTrack;
	private boolean loading;
	private boolean rising;
	private int task;
	private int load;
	private int par;
	private int strength;

	/**
	 * creates a new GolfPlayer that can be added to a golf track
	 *
	 * @param player    the uuid of the player that should be added
	 * @param golfball  the golfball as an Item that can be used by the player
	 * @param golfTrack the golf track the player want's to play at
	 */
	public GolfPlayer(UUID player, Item golfball, GolfTrack golfTrack) {
		// initialising instance variables
		this.player = player;
		this.golfball = golfball;
		this.golfTrack = golfTrack;
		this.strength = 1;
		this.par = 0;
		this.loading = false;
		this.rising = true;
	}

	/**
	 * asynchronous task that displays a small particle animation at the player's
	 * golf ball (so that one is able to identify which golf ball is your's)
	 */
	@SuppressWarnings("deprecation")
	public void startBallParticles() {
		// starting asynchronous task
		this.task = Bukkit.getScheduler().scheduleAsyncRepeatingTask(Minigolf.getInstance(), () -> {
			// displaying particle effect to current player
			Bukkit.getPlayer(player).spigot().playEffect(golfball.getLocation(), Effect.WITCH_MAGIC, 1, 1, 0.0F, 0.0F,
					0.0F, 0.1F, 10, 8);
		}, 20, 20); // repeating every 3 seconds
	}

	/**
	 * asynchronous task that loads up the strength the player is about to shoot at
	 */
	@SuppressWarnings("deprecation")
	public void loadStrength() {
		this.loading = true; // now loading

		this.load = Bukkit.getScheduler().scheduleAsyncRepeatingTask(Minigolf.getInstance(), () -> {
			// displaying current strength in player's actionbar and title
			ActionbarAPI.sendActionbar(Bukkit.getPlayer(player), getLoadDisplay());
			TitleAPI.sendTitle(Bukkit.getPlayer(player), 0, 30, 20, "", getLoadDisplay());
			Bukkit.getPlayer(player).playSound(Bukkit.getPlayer(player).getLocation(), Sound.CLICK, 5, 1);

			// if strength is at maximum, the strength goes down
			if (rising) {
				setStrength(getStrength() + 1);
				if (getStrength() > 4)
					rising = false;
			} else {
				setStrength(getStrength() - 1);

				if (getStrength() < 1)
					rising = true;
			}
		}, 0, 10); // repeating twice in a second
	}

	/**
	 * @return a string that displays the strength the player currently loaded up
	 */
	public String getLoadDisplay() {
		String display = "";
		// the current strength
		for (int i = 0; i < strength; i++) {
			display += ChatColor.GREEN + "▌";
		}
		if (strength < 5) { // max strength
			int add = 5 - strength;
			// the rest
			for (int i = 0; i < add; i++) {
				display += ChatColor.GRAY + "▌";
			}
		}
		return display;
	}

	/**
	 * let's a player shoot his golf ball
	 *
	 * @param vector the vector the ball should be shot with
	 */
	public void shoot(Vector vector) {
		// cancelling loading task
		this.cancelLoading();

		golfball.setVelocity(vector);

		// resetting strength
		this.setStrength(1);
		// adding one shot
		this.setPar(getPar() + 1);
	}

	/**
	 * cancels loading task (e.g. when player shoots his ball)
	 */
	public void cancelLoading() {
		this.loading = false;
		Bukkit.getPlayer(player).playSound(Bukkit.getPlayer(player).getLocation(), Sound.LEVEL_UP, 5, 1);
		Bukkit.getScheduler().cancelTask(this.load);
	}

	/**
	 * cancels particle task (e.g. when player leaves the track)
	 */
	public void stopBallParticles() {
		Bukkit.getScheduler().cancelTask(this.task);
	}

	// ------------ start getters and setters ------------
	public void setStrength(int strength) {
		this.strength = strength;
	}

	public void setPar(int par) {
		this.par = par;
	}

	public UUID getPlayer() {
		return this.player;
	}

	public Item getGolfball() {
		return this.golfball;
	}

	public int getStrength() {
		return this.strength;
	}

	public int getPar() {
		return this.par;
	}

	public boolean isLoading() {
		return this.loading;
	}

	public GolfTrack getGolfTrack() {
		return this.golfTrack;
	}
	// ------------ end getters and setters ------------
}
