package de.vuquiz.minigolf.util;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworkBuilder {

    /**
     * builds and spawns a firework
     *
     * @param player the player who should receive the firework
     */
    public static void build(Player player) {
        // spawning the firework
        Firework firework = player.getWorld().spawn(player.getLocation(), Firework.class);
        FireworkEffect effect = FireworkEffect.builder() // editing firework with specified settings
                .withColor(Color.AQUA)
                .flicker(true)
                .trail(true)
                .withFade(Color.RED)
                .with(FireworkEffect.Type.STAR)
                .build();

        // submitting new firework meta
        FireworkMeta meta = firework.getFireworkMeta();
        meta.addEffect(effect);
        meta.setPower(1);
        firework.setFireworkMeta(meta);
    }
}