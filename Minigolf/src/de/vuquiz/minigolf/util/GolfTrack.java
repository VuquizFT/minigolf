package de.vuquiz.minigolf.util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import de.vuquiz.minigolf.Minigolf;
import de.vuquiz.minigolf.api.ActionbarAPI;
import de.vuquiz.minigolf.api.ItemBuilder;
import de.vuquiz.minigolf.api.TitleAPI;
import net.md_5.bungee.api.ChatColor;

public class GolfTrack {

	// instance variables
	private int id;
	private int par;
	private Location start;
	private Location hole;
	private Location zone1;
	private Location zone2;
	private Cuboid zone;
	private List<GolfPlayer> players;
	private List<UUID> ignore;

	/**
	 * creates a new golf track with the given id
	 *
	 * @param id the id/number of the track
	 */
	public GolfTrack(int id) {
		// initialising instance variables
		this.id = id;
		this.players = new ArrayList<GolfPlayer>();
		this.ignore = new ArrayList<UUID>();
	}

	/**
	 * add's a player to the current golf track
	 *
	 * @param player the player that should be added
	 */
	public void addPlayer(Player player) {
		// player cannot be added again if he already "achieved" the hole
		if (!this.ignore.contains(player.getUniqueId())) {

			// adding golfball synchronously
			Bukkit.getScheduler().runTask(Minigolf.getInstance(), () -> {
				// adding player to golf track
				final Location startCopy = start.clone();
				// spawning golf ball not directly at start location (otherwise one could not
				// identify his own ball if too many players are using this track)
				Item golfball = Bukkit.getWorld(player.getLocation().getWorld().getName()).dropItemNaturally(startCopy
						.add(Minigolf.getInstance().getRandom(0.0, 1.0), 0, Minigolf.getInstance().getRandom(0.0, 1.0)),
						Minigolf.getGolfBall());
				golfball.setPickupDelay(Integer.MAX_VALUE);
				golfball.setVelocity(new Vector(0, 0, 0));
				if (golfball != null) { // if everything went right
					// adding player with his ball to the current track
					players.add(new GolfPlayer(player.getUniqueId(), golfball, GolfTrack.this));
					// starting particle animation task
					getGolfPlayer(player).startBallParticles();
				}
			});

			// adding golf club to player's inventory
			player.playSound(player.getLocation(), Sound.LEVEL_UP, 5, 1);
			player.getInventory()
					.addItem(new ItemBuilder(Material.STICK).setName(ChatColor.GOLD + "Golfschläger").toItemStack());
			// some messaging
			final String message = ChatColor.GRAY + "Du spielst nun auf der Bahn " + ChatColor.GOLD
					+ String.valueOf(this.id) + ChatColor.GRAY + ", Par: " + ChatColor.GOLD + String.valueOf(this.par);
			player.sendMessage(Minigolf.getPrefix() + message);
			ActionbarAPI.sendActionbar(player, message);
		}
	}

	/**
	 * removes a player from the current golf track
	 *
	 * @param player the player that should be removed
	 */
	public void removePlayer(Player player) {
		// the golf player object of the given player
		final GolfPlayer golfPlayer = this.getGolfPlayer(player);
		// if player is currently registered at a golf track
		if (golfPlayer != null) {
			// removing player from golf track; canceling his tasks
			this.players.remove(golfPlayer);
			golfPlayer.getGolfball().remove();
			golfPlayer.stopBallParticles();
			golfPlayer.cancelLoading();
		}
		// removing golf club from player's inventory
		player.playSound(player.getLocation(), Sound.ANVIL_USE, 1, 1);
		player.getInventory()
				.remove(new ItemBuilder(Material.STICK).setName(ChatColor.GOLD + "Golfschläger").toItemStack());

		// some messaging
		String message = ChatColor.GRAY + "Du hast deine aktuelle Bahn verlassen.";
		player.sendMessage(Minigolf.getPrefix() + message);
		ActionbarAPI.sendActionbar(player, message);
	}
	
	/**
	 * @param player the corresponding player
	 * @return the golf player object of a player
	 */
	public GolfPlayer getGolfPlayer(Player player) {
		return this.players
				.stream()
				.filter(golfPlayers -> golfPlayers.getPlayer().equals(player.getUniqueId()))
				.findAny().orElse(null); // returning golf player object if found
	}

	/**
	 * checks asynchronously if a player has achieved the hole with his ball
	 */
	@SuppressWarnings("deprecation")
	public void startHoleCheck() {
		// starting hole check
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(Minigolf.getInstance(), () -> {
			// iterating through all players currently using this track
			players.forEach(golfPlayers -> {
				// the player object
				final Player player = Bukkit.getPlayer(golfPlayers.getPlayer());
				// ignoring player if he has already achieved the hole before
				if (!ignore.contains(player.getUniqueId())) {
					// if player has achieved the hole
					if (golfPlayers.getGolfball().getLocation().distance(hole) <= 1) {
						player.sendMessage(Minigolf.getPrefix() + ChatColor.GRAY + "Du hast die Bahn " + ChatColor.GOLD
								+ String.valueOf(getID()) + ChatColor.GRAY + " mit " + ChatColor.GREEN
								+ String.valueOf(golfPlayers.getPar()) + ChatColor.GRAY + "/" + ChatColor.RED
								+ String.valueOf(getPar()) + ChatColor.GRAY + " Schlägen geschafft!");
						// let's the golfball jump up for a moment (> small animation)
						golfPlayers.getGolfball().setVelocity(new Vector(0, 1, 0));
						// spawning a firework synchronously
						Bukkit.getScheduler().runTask(Minigolf.getInstance(), () -> {
							FireworkBuilder.build(player);
							ignore.add(player.getUniqueId());
						});
					}
				}
			});
		}, 20, 20); // repeating every second
	}

	/**
	 * checks asynchronously if a ball has went out of range or in water
	 */
	@SuppressWarnings("deprecation")
	public void startOutOfRangeCheck() {
		// starting check
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(Minigolf.getInstance(), () -> {
			// iterating through all players currently using this track
			players.forEach(golfPlayers -> {
				// the player object
				final Player player = Bukkit.getPlayer(golfPlayers.getPlayer());
				// the location of the player's ball
				final Location ballLocation = golfPlayers.getGolfball().getLocation();

				// if ball went out of range or in water
				if (!golfPlayers.getGolfTrack().isInZone(ballLocation)
						|| ballLocation.getBlock().getType() == Material.STATIONARY_WATER) {
					// teleporting golf ball not directly at start location (otherwise one could not
					// identify his own ball if too many players are using this track)
					golfPlayers.getGolfball().setVelocity(new Vector(0, 0, 0));
					final Location startCopy = start.clone();
					golfPlayers.getGolfball().teleport(startCopy.add(Minigolf.getInstance().getRandom(0.0, 1.0), 0,
							Minigolf.getInstance().getRandom(0.0, 1.0)));

					// some messaging
					player.playSound(player.getLocation(), Sound.ANVIL_USE, 1, 1);
					player.sendMessage(Minigolf.getPrefix()
							+ "Leider hast du deinen Golfball außerhalb der Bahn geschossen!\nEr wurde wieder zum Start teleportiert.");
					ActionbarAPI.sendActionbar(player, ChatColor.RED + "Out of bounds!");
					TitleAPI.sendTitle(player, 20, 40, 30, ChatColor.RED + "Out of bounds!", "");
				}
			});
		}, 30, 30); // repeating every 1 1/2 seconds
	}

	/**
	 * @param location the location that should be checked
	 * @return if the locations is located in the golf track zone
	 */
	public boolean isInZone(Location location) {
		return this.zone.contains(location);
	}

	// ----------- start getters and setters ------------
	public void setStart(Location start) {
		this.start = start;
	}

	public void setHole(Location hole) {
		this.hole = hole;
	}

	public void setZone1(Location zone) {
		this.zone1 = zone;
	}

	public void setZone2(Location zone) {
		this.zone2 = zone;
	}

	public void calculateTotalZone() {
		zone = new Cuboid(zone1, zone2);
	}

	public void setPar(int par) {
		this.par = par;
	}

	public int getID() {
		return this.id;
	}

	public Location getZone1() {
		return this.zone1;
	}

	public Location getZone2() {
		return this.zone2;
	}

	public Cuboid getTotalZone() {
		return this.zone;
	}

	public int getPar() {
		return this.par;
	}

	public Location getStart() {
		return this.start;
	}

	public Location getHole() {
		return this.hole;
	}

	public List<GolfPlayer> getPlayers() {
		return this.players;
	}

	public List<UUID> getIgnore() {
		return this.ignore;
	}

	// ----------- end getters and setters ------------
}