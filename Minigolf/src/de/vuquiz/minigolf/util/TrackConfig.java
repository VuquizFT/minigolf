package de.vuquiz.minigolf.util;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class TrackConfig {

    // instance variables
    private File file;
    private FileConfiguration fileConfiguration;

    /**
     * Constructor for objects of the class TrackConfig
     */
    public TrackConfig() {
        // initialising instance variables
        this.file = new File("plugins/Minigolf", "golftracks.yml");
        this.fileConfiguration = YamlConfiguration.loadConfiguration(file);
    }

    /**
     * saves the given start location in a configuration file
     *
     * @param trackId       the id of the corresponding track
     * @param startLocation the start location of the track
     */
    public void saveStart(int trackId, Location startLocation) {
        // saving corresponding track data in file
        this.fileConfiguration.set("Start." + trackId, startLocation);

        // saving changes
        this.save();
    }

    /**
     * saves the given hole location in a configuration file
     *
     * @param trackId      the id of the corresponding track
     * @param holeLocation the hole location of the track
     */
    public void saveHole(int trackId, Location holeLocation) {
        // saving corresponding track data in file
        this.fileConfiguration.set("Hole." + trackId, holeLocation);

        // saving changes
        this.save();
    }

    /**
     * saves the given par in a configuration file
     *
     * @param trackId the id of the corresponding track
     * @param par     the par amount for the given golf track
     */
    public void savePar(int trackId, int par) {
        // saving corresponding track data in file
        this.fileConfiguration.set("Par." + trackId, par);

        // saving changes
        this.save();
    }

    /**
     * saves the first zone in a configuration file
     *
     * @param trackId the id of the corresponding track
     * @param zoneId  the id of the zone (should be 1 or 2)
     * @param zone    the first location
     */
    public void saveZone(int trackId, int zoneId, Location zone) {
        // saving corresponding track data in file
        this.fileConfiguration.set("Zone" + zoneId + "." + trackId, zone);

        // saving changes
        this.save();
    }

    /**
     * saves changes from track configuration
     */
    private void save() {
        // saving changes
        try {
            this.fileConfiguration.save(this.file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ----------- start getters -------------
    public File getFile() {
        return this.file;
    }

    public FileConfiguration getFileConfiguration() {
        return this.fileConfiguration;
    }
    // ---------- end getters -----------
}