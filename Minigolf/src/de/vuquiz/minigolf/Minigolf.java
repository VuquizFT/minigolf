package de.vuquiz.minigolf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.reflect.ClassPath;

import de.vuquiz.minigolf.commands.RLTracksCommand;
import de.vuquiz.minigolf.commands.SetHoleCommand;
import de.vuquiz.minigolf.commands.SetParCommand;
import de.vuquiz.minigolf.commands.SetStartCommand;
import de.vuquiz.minigolf.commands.SetZoneCommand;
import de.vuquiz.minigolf.skull.Skull;
import de.vuquiz.minigolf.util.GolfPlayer;
import de.vuquiz.minigolf.util.GolfTrack;
import de.vuquiz.minigolf.util.TrackConfig;

public class Minigolf extends JavaPlugin {

	// instance variables
	private static Minigolf instance;
	private static TrackConfig trackConfig;
	private static List<GolfTrack> golfTracks;
	private static ItemStack golfBall;
	private static String prefix;
	private Random random;

	@Override
	public void onLoad() {
		// initialising instance variables
		instance = this;
		prefix = ChatColor.GRAY + "[" + ChatColor.GREEN + "Minigolf" + ChatColor.GRAY + "] ";
	}

	@Override
	public void onEnable() {
		// registering commands
		this.getCommand("rltracks").setExecutor(new RLTracksCommand());
		this.getCommand("sethole").setExecutor(new SetHoleCommand());
		this.getCommand("setpar").setExecutor(new SetParCommand());
		this.getCommand("setstart").setExecutor(new SetStartCommand());
		this.getCommand("setzone").setExecutor(new SetZoneCommand());

		// registering events
		try { // iterating through all listener classes
			for (ClassPath.ClassInfo classInfo : ClassPath.from(getClassLoader())
					.getTopLevelClasses("de.vuquiz.minigolf.listener")) {
				Class<?> currentClass = Class.forName(classInfo.getName());

				if (Listener.class.isAssignableFrom(currentClass))
					Bukkit.getPluginManager().registerEvents((Listener) currentClass.newInstance(), this);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		// initialising track config
		trackConfig = new TrackConfig();
		random = new Random();

		// loading golf tracks
		loadTracks();

		// load players to corresponding tracks
		this.loadPlayersInTrack();

		// loading golf ball texture
		golfBall = Skull.getCustomSkull(
				"http://textures.minecraft.net/texture/c6e4f196e908a862353dcbb8edc69fc6b0f288f0e2c4bea72f09bff698753");
	}

	/**
	 * (re)loads all existing golf tracks into array list
	 */
	public static void loadTracks() {
		// initialising golf track list
		golfTracks = new ArrayList<GolfTrack>();

		// creating tracks file if not already existing
		if (!getTrackConfig().getFile().exists())
			getTrackConfig().getFile().mkdir();

		// loading all tracks from configuration into a locally stored object
		if (getTrackConfig().getFileConfiguration().getConfigurationSection("Start") != null) {
			// in config saved tracks
			for (String tracks : getTrackConfig().getFileConfiguration().getConfigurationSection("Start")
					.getKeys(false)) {
				// creating new track with stored data
				final GolfTrack golfTrack = new GolfTrack(Integer.valueOf(tracks));
				golfTrack
						.setStart((Location) getTrackConfig().getFileConfiguration().get("Start." + golfTrack.getID()));
				if (getTrackConfig().getFileConfiguration().get("Hole." + golfTrack.getID()) != null)
					golfTrack.setHole(
							(Location) getTrackConfig().getFileConfiguration().get("Hole." + golfTrack.getID()));
				if (getTrackConfig().getFileConfiguration().get("Par." + golfTrack.getID()) != null)
					golfTrack.setPar((int) getTrackConfig().getFileConfiguration().get("Par." + golfTrack.getID()));
				if (getTrackConfig().getFileConfiguration().get("Zone1." + golfTrack.getID()) != null)
					golfTrack.setZone1(
							(Location) getTrackConfig().getFileConfiguration().get("Zone1." + golfTrack.getID()));
				if (getTrackConfig().getFileConfiguration().get("Zone2." + golfTrack.getID()) != null)
					golfTrack.setZone2(
							(Location) getTrackConfig().getFileConfiguration().get("Zone2." + golfTrack.getID()));
				// calculating available zone for current golf track
				golfTrack.calculateTotalZone();
				golfTrack.startHoleCheck();
				golfTrack.startOutOfRangeCheck();
				// adding track to global track list
				golfTracks.add(golfTrack);
			}
		}
	}

	/**
	 * @param id the id of the golf track
	 * @return the corresponding golf track as an object
	 */
	public static GolfTrack getGolfTrack(int id) {
		return Minigolf.getGolfTracks()
				.stream()
				.filter(tracks -> tracks.getID() == id) // searching for track with given id
				.findAny().orElse(null); // returning track if existing
	}

	/**
	 * @param player the corresponding player
	 * @return the golf player object of a player
	 */
	public GolfPlayer getGolfplayer(Player player) {
		return Minigolf.getGolfTracks()
				.stream()
				.map(GolfTrack::getPlayers)
				.flatMap(Collection::stream)
				.filter(p -> p.getPlayer().equals(player.getUniqueId()))
				.findAny().orElse(null);
	}

	/**
	 * @param min the minimum double value that should be returned
	 * @param max the maximum double value that should be returned
	 * @return a random double value within min and max range
	 */
	public double getRandom(double min, double max) {
		return min + (max - min) * random.nextDouble();
	}

	/**
	 * adding online players synchronously to golf tracks if they are close to them;
	 * removing them if they've left the track area
	 */
	private void loadPlayersInTrack() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
			// iterating through all golf tracks
			getGolfTracks().forEach(tracks -> {
				// iterating through all online players
				Bukkit.getOnlinePlayers().forEach(all -> {
					// if track zone is already set
					if (tracks.getTotalZone() != null) {
						// if player is close to tracks's start location
						if (tracks.isInZone(all.getLocation())) {
							// if player is not already using this track
							if (tracks.getGolfPlayer(all) == null)
								// adding player to current track
								tracks.addPlayer(all);
						} else { // player is no longer in the track area
							if (tracks.getGolfPlayer(all) != null)
								// removing player from current track
								tracks.removePlayer(all);
							if (tracks.getIgnore().contains(all.getUniqueId()))
								tracks.getIgnore().remove(all.getUniqueId());
						}
					}
				});
			});
		}, 40, 40); // repeating every 2 seconds
	}

	/**
	 * @return the minigolf plugin instance
	 */
	public static Minigolf getInstance() {
		return instance;
	}

	/**
	 * @return the golfball with corresponding texture
	 */
	public static ItemStack getGolfBall() {
		return golfBall;
	}

	/**
	 * @return the track config
	 */
	public static TrackConfig getTrackConfig() {
		return trackConfig;
	}

	/**
	 * @return the minigolf prefix for chat messaging
	 */
	public static String getPrefix() {
		return prefix;
	}

	/**
	 * @return all registered golf tracks
	 */
	public static List<GolfTrack> getGolfTracks() {
		return golfTracks;
	}
}